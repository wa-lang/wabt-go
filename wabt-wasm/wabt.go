// 版权 @2022 凹语言 作者。保留所有权利。

// wabt wasm 包
package wabt_wasm

import _ "embed"

// Wabt 版本号
const Version = "1.0.37"

//go:embed internal/wabt-1.0.37-wasm/wasm-strip.wasm
var wasm_strip string

//go:embed internal/wabt-1.0.37-wasm/wasm-validate.wasm
var wasm_validate string

//go:embed internal/wabt-1.0.37-wasm/wat2wasm.wasm
var wat2wasm string

// 读取 wat2wasm.wasm 文件
func LoadWat2Wasm() string {
	return wat2wasm
}

// 读取 wasm-validate.wasm 文件
func LoadWasmValidate() string {
	return wasm_validate
}

// 读取 wasm-strip.wasm 文件
func LoadWasmStrip() string {
	return wasm_strip
}
